using System;

namespace api_identity
{
    public static class DateTimeExtensions
    {
        public static long GetMillisecondsTimeStamp(this DateTime dateTime)
        {
            DateTimeOffset dto = dateTime;
            return dto.ToUnixTimeMilliseconds();
        }

        public static long GetSecondsTimeStamp(this DateTime dateTime)
        {
            DateTimeOffset dto = dateTime;
            return dto.ToUnixTimeSeconds();
        }

        public static DateTime GetDateTimeFromSecondsTimeStamp(this long timeStamp)
        {
            return DateTimeOffset.FromUnixTimeSeconds(timeStamp).LocalDateTime;
        }

        public static DateTime GetDateTimeFromMilliSecondsTimeStamp(this long timeStamp)
        {
            //默认为UTC时间
            DateTimeOffset dto = DateTimeOffset.FromUnixTimeMilliseconds(timeStamp);
            //转为本地时区
            return dto.LocalDateTime;
        }

        public static string GetChineseShortDate(this DateTime dateTime)
        {//2020年5月28日
            return $"{dateTime:D}";
        }
    }
}
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace api_identity.Controllers
{
    public class TestController : BasicAuthorizeController
    {
        public TestController()
        {

        }

        [AllowAnonymous]
        [HttpGet("noauth")]
        public string GetNoAuth()
        {
            return nameof(GetNoAuth);
        }

        [HttpGet("auth")]
        public string GetAuth()
        {
            return nameof(GetAuth);
        }
    }
}
﻿using api_identity.Models.Response;
using Microsoft.AspNetCore.Mvc;

namespace api_identity.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BasicController : ControllerBase
    {
        protected IActionResult Result(bool succeed, string msg)
        {
            return succeed ? this.OKResult(msg) : this.FailResult(msg);
        }

        protected IActionResult OKResult(string msg = default)
        {
            return Ok(new ResponseResult
            {
                Code = ResponseCode.Succeed,
                Msg = msg ?? "success"
            });
        }

        protected IActionResult OKResultOfT<T>(T data, string msg = default)
        {
            return Ok(new ResponseResultOfT<T>
            {
                Code = ResponseCode.Succeed,
                Msg = msg ?? "success",
                Data = data
            });
        }
        protected IActionResult NotFoundResult(string msg = default)
        {
            return NotFound(new ResponseResult
            {
                Code = ResponseCode.Failed,
                Msg = msg ?? "not found"
            });
        }
        protected IActionResult FailResult(string msg = default)
        {
            return BadRequest(new ResponseResult
            {
                Code = ResponseCode.Failed,
                Msg = msg ?? "fail"
            });
        }
    }
}

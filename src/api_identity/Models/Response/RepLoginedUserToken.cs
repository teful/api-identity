namespace api_identity.Models.Response
{
    public class RepLoginedUserToken
    {
        public string AccessToken { get; set; }
        public int ExpiresSeconds { get; set; }
        public string RefreshToken { get; set; }
    }
}
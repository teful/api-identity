namespace api_identity.Models.Response
{
    public class ResponseResult
    {
        public ResponseCode Code { get; set; }
        public string Msg { get; set; }
    }
}
namespace api_identity.Models.Response
{
    public class ResponseResultOfT<T>:ResponseResult
    {
        public T Data { get; set; }
    }
}
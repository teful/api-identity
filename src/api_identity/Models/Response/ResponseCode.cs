namespace api_identity.Models.Response
{
    public enum ResponseCode : byte
    {
        Succeed = 1,
        Failed
    }
}
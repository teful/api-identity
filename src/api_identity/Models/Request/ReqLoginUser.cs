﻿namespace api_identity.Models.Request
{
    public class ReqLoginUser
    {
        public string Email { get; set; }
        public string PassWord { get; set; }
    }
}

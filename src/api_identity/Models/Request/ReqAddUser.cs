namespace api_identity.Models.Request
{
    public class ReqAddUser
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Pwd { get; set; }
    }
}
﻿namespace api_identity.Options
{
    public class AccountOptions
    {
        #region 密码

        /// <summary>
        /// 密码包含数字
        /// </summary>
        public bool PwdContainNum { get; set; } = true;
        /// <summary>
        /// 密码包含大写字母
        /// </summary>
        public bool PwdContainUpperCase { get; set; } = true;
        /// <summary>
        /// 密码包含小写字母
        /// </summary>
        public bool PwdContainLowerCase { get; set; } = true;
        /// <summary>
        /// 密码包含非数字和字母
        /// </summary>
        public bool PwdContainNonAlphanumeric { get; set; } = true;
        /// <summary>
        /// 密码最小长度
        /// </summary>
        public int PwdMinLength { get; set; } = 6;

        #endregion

        /// <summary>
        /// 新用户锁定
        /// </summary>
        public bool LockoutNewUser { get; set; } = false;
        /// <summary>
        /// 失败多少次数后锁定(密码错误)
        /// </summary>
        public int LockoutMaxFailedAttempts { get; set; } = 5;
        /// <summary>
        /// 邮箱唯一
        /// </summary>
        public bool UniqueEmail { get; set; } = true;
        /// <summary>
        /// 锁定时长(分)
        /// </summary>
        public int LockoutMinutes { get; set; } = 10;
    }
}

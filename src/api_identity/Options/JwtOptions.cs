﻿namespace api_identity.Options
{
    public class JwtOptions
    {
        public string Key { get; set; } = "123456789123456789";
        public string Issuer { get; set; } = "http://localhost:5000";
        public int ExpireSeconds { get; set; } = 300;
    }
}

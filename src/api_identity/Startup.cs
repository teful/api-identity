using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api_identity.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace api_identity
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlite(this.Configuration.GetConnectionString("DefaultConnection"));
            });

            services.AddIdentity(() =>
            {
                return this.Configuration.GetSection(nameof(Options.AccountOptions)).Get<Options.AccountOptions>();
            });
            services.AddJwtAuthentication(() =>
            {
                return this.Configuration.GetSection(nameof(Options.JwtOptions)).Get<Options.JwtOptions>();
            });
            services.AddPolicyAuthorization();

            services.AddControllers();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthentication();//认证
            app.UseAuthorization();//授权

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

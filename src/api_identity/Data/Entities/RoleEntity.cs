﻿using Microsoft.AspNetCore.Identity;

namespace api_identity.Data.Entities
{
    /// <summary>
    /// 角色实体
    /// </summary>
    public class RoleEntity : IdentityRole
    {
    }
}

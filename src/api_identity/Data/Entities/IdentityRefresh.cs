using System;

namespace api_identity.Data.Entities
{
    public class IdentityRefresh<TKey, UserKey> where TKey : IEquatable<TKey> where UserKey : IEquatable<TKey>
    {
        public virtual TKey Id { get; set; }
        public virtual UserKey UserId { get; set; }
        public virtual string Key { get; set; }
        public virtual long ExpireTimeStamp { get; set; }
        public virtual string ConcurrencyStamp { get; set; } = Guid.NewGuid().ToString();

    }
}
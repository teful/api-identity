using System;

namespace api_identity.Data.Entities
{
    public class RefreshTokenEntity : IdentityRefresh<string, string>
    {
        public RefreshTokenEntity()
        {
            this.Id = Guid.NewGuid().ToString();
        }

        public RefreshTokenEntity(string userId, string key, DateTime expireTime)
        : this()
        {
            this.UserId = userId;
            this.Key = key;
            this.ExpireTimeStamp = expireTime.GetMillisecondsTimeStamp();
        }
    }
}
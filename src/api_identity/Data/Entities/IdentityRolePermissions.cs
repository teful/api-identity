﻿using System;

namespace api_identity.Data.Entities
{
    /// <summary>
    /// 身份角色权限
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    public class IdentityRolePermissions<TKey> where TKey : IEquatable<TKey>
    {
        public virtual TKey RoleId { get; set; }
        public virtual TKey PermissionsId { get; set; }
    }
}

﻿using System;

namespace api_identity.Data.Entities
{
    /// <summary>
    /// 权限实体
    /// </summary>
    public class PermissionsEntity : IdentityPermissions<string>
    {
        public PermissionsEntity(string controllerName, string actionName, string id = default)
            : base(controllerName, actionName)
        {
            this.Id = id?? Guid.NewGuid().ToString();
        }

    }
}

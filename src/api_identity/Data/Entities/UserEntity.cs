﻿using Microsoft.AspNetCore.Identity;

namespace api_identity.Data.Entities
{
    /// <summary>
    /// 用户实体
    /// </summary>
    public class UserEntity : IdentityUser
    {
    }
}
